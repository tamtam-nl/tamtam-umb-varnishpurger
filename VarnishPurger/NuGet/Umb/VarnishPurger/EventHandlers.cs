﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Events;
using Umbraco.Core.Models;
using Umbraco.Core.Publishing;
using Umbraco.Core.Services;

namespace TamTam.NuGet.Umb.VarnishPurger
{
    public class EventHandlers : ApplicationEventHandler
    {
        public EventHandlers(){
            ContentService.Published += ContentServiceOnPublished;
            ContentService.Moved += ContentServiceOnMoved;
            ContentService.UnPublished += ContentServiceOnUnPublished;
            ContentService.Deleted += ContentServiceOnDeleted;
        }

        private void ContentServiceOnMoved(IContentService sender, MoveEventArgs<IContent> moveEventArgs){
            Helper.Purge();
        }

        private void ContentServiceOnDeleted(IContentService sender, DeleteEventArgs<IContent> deleteEventArgs){
            Helper.Purge();
        }

        private void ContentServiceOnUnPublished(IPublishingStrategy sender, PublishEventArgs<IContent> publishEventArgs){
            Helper.Purge();
        }

        private void ContentServiceOnPublished(IPublishingStrategy sender, PublishEventArgs<IContent> publishEventArgs){
            Helper.Purge();
        }
    }
}