﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace TamTam.NuGet.Umb.VarnishPurger
{
    public static class Helper
    {
        public static void Purge() {
            var proxies = ConfigurationManager.AppSettings["VarnishDomains"].Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string proxy in proxies){
                string url = "http://" + proxy + "/.*/([?].*)?$";
                PurgeUrl(url);
                url = "http://" + proxy + "/api/.*";
                PurgeUrl(url);
            }

        }

        public static void Purge(string regex)
        {
            var proxies = ConfigurationManager.AppSettings["VarnishDomains"].Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string proxy in proxies)
            {
                var url = "http://" + proxy + regex;
                PurgeUrl(url);
            }
        }

        private static void PurgeUrl(string url){
            var req = WebRequest.Create(url) as HttpWebRequest;
            req.Method = "PURGE";
            req.Timeout = 1000; // 1 second is plenty;
            req.Host = ConfigurationManager.AppSettings["VarnishDomainToPurge"];
            try{
                req.GetResponse();
            }
            catch (WebException ex){
                Elmah.ErrorSignal.FromCurrentContext().Raise(new ApplicationException("Purge error on " + req.Host + " for " + url, ex));
            }
        }
    }
}